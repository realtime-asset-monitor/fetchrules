// Copyright 2024 Google LLC
//
// Licensed under the Apache License, Version 2.0 (the 'License');
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an 'AS IS' BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//.

package fetchrules

import (
	"context"
	"encoding/csv"
	"flag"
	"fmt"
	"os"
	"path/filepath"

	"cloud.google.com/go/storage"
	jsoniter "github.com/json-iterator/go"
)

// DeployRules read rule files packages them an deploy to firestore collection
func DeployRules(ctx context.Context,
	rulesFolderPath string,
	onlyType string,
	rulesRepoBucketName string,
	projectID string,
	logOnlySeverityLevels string,
	redeploy bool,
	location string) (err error) {
	var verbose bool
	var forceRefreshRuleCache bool

	// Avoid flag redefine error during test
	if len(rulesFolderPath+onlyType+rulesRepoBucketName+projectID+logOnlySeverityLevels) == 0 {
		flag.StringVar(&rulesFolderPath, "path", "./rules", "path to the folder containing all rule files default is ./rules")
		flag.StringVar(&onlyType, "type", "", "one specific content type like IAM_POLICY of asset type like cloudfunctions.googleapis.com/CloudFunction")
		flag.BoolVar(&forceRefreshRuleCache, "force-refresh-rule-cache", false, "will deploy a new revision of fetchrule cloud Run service with the same settings so that all active instances are forced to terminate meaning in memory rules cache is wiped out, forcing to read rule from GCS again")
		flag.StringVar(&location, "location", "europe-west1", "Cloud Run location where is already deployed fetchrules microservice")
		flag.Parse()
		rulesRepoBucketName = global.serviceEnv.RulesRepoBucketName
		projectID = global.serviceEnv.ProjectID

		verbose = true
		fmt.Printf("ProjectID %s\n", projectID)
		fmt.Printf("Environment %s\n", global.serviceEnv.Environment)
		fmt.Printf("LogOnlySeveritylevels %s\n", global.serviceEnv.LogOnlySeveritylevels)
		fmt.Printf("RulesRepoBucketName %s\n", rulesRepoBucketName)
		fmt.Printf("RulesFolderPath %s\n", rulesFolderPath)
		if onlyType != "" {
			fmt.Printf("OnlyType %s\n", onlyType)
		}
		fmt.Printf("\n")
	}
	if !redeploy {
		if forceRefreshRuleCache {
			redeploy = true
		}
	}

	constraints, rules, readmes, err := parseRuleFiles(rulesFolderPath)
	if err != nil {
		return fmt.Errorf("error when parsing rules %v", err)
	}
	if verbose {
		fmt.Printf("parseRuleFiles completed without error %d rules %d constraints %d readmes\n", len(rules), len(constraints), len(readmes))
	}

	rulesRepo := makeRulesRepo(onlyType, constraints, rules, readmes)
	if verbose {
		fmt.Println("rulesRepo built")
	}

	b, err := jsoniter.MarshalIndent(rulesRepo, "", "  ")
	if err != nil {
		return fmt.Errorf("jsoniter.MarshalIndent %v", err)
	}

	err = os.WriteFile(fmt.Sprintf("%s/rulerepo.json", rulesFolderPath), b, 0600) // prevent G306 Expect WriteFile permissions to be 0600 or less
	if err != nil {
		return fmt.Errorf("error when writing rulerepo.json file %v", err)
	}
	if verbose {
		fmt.Printf("rulerepo.json file written locally %d asset or content type(s)\n", len(rulesRepo.ContentOrAssetTypeConfigs))
	}

	records := [][]string{
		{
			"contentOrAssetType", "ruleName", "constraintName", "severity", "category", "description",
		},
	}

	for contentOrAssetType, ruleConfigs := range rulesRepo.ContentOrAssetTypeConfigs {
		for ruleName, ruleConfig := range ruleConfigs {
			for constraintName, constraintConfig := range ruleConfig.ConstraintConfigs {
				var category, description string
				var categoryInterface interface{} = constraintConfig.Metadata.Annotations["category"]
				category = categoryInterface.(string)
				var descriptionInterface interface{} = constraintConfig.Metadata.Annotations["description"]
				description = descriptionInterface.(string)
				record := []string{contentOrAssetType,
					ruleName,
					constraintName,
					constraintConfig.Spec.Severity,
					category,
					description}
				records = append(records, record)
			}
		}
	}
	path := filepath.Clean(fmt.Sprintf("%s/rulerepo.csv", rulesFolderPath)) // prevent G304
	err = os.Truncate(path, 0)
	if err != nil {
		return fmt.Errorf("error truncating the csv file %v", err)
	}

	file, err := os.OpenFile(path, os.O_CREATE|os.O_WRONLY, 0600)
	defer func() {
		if err := file.Close(); err != nil {
			fmt.Printf("Error closing file: %s\n", err)
		}
	}()
	if err != nil {
		return fmt.Errorf("error opening the csv file %v", err)
	}
	csvWriter := csv.NewWriter(file)
	err = csvWriter.WriteAll(records)
	if err != nil {
		return fmt.Errorf("error writing the csv file %v", err)
	}
	csvWriter.Flush()
	if verbose {
		fmt.Printf("rulerepo.csv file written locally %d lines", len(records))
	}

	storageClient, err := storage.NewClient(ctx)
	if err != nil {
		return fmt.Errorf("storage.NewClient %v", err)
	}
	rulesRepoBucket := storageClient.Bucket(rulesRepoBucketName)
	if verbose {
		fmt.Println("\nGot rulesRepoBucket handle without error")
	}

	err = updateRules2gcs(ctx, onlyType, rulesRepo, rulesRepoBucket, global.serviceEnv.LogOnlySeveritylevels)
	if err != nil {
		return fmt.Errorf("error when updating rules to gcs %v", err)
	}
	if verbose {
		fmt.Printf("rulesRepo updated to gcs bucket %s\n", rulesRepoBucketName)
	}

	if redeploy {
		fmt.Println("Start deploying a new revision of fetchrule cloud Run service with the same settings so that all active instances are forced to terminate meaning in memory rules cache is wiped out, forcing to read rule from GCS again")
		cRunSrv, err := redeployCloudRunRevision(ctx,
			projectID,
			location)
		if err != nil {
			return err
		}
		fmt.Printf("%s redeployed %s", microserviceName, cRunSrv.LatestCreatedRevision)
	}
	return nil
}
