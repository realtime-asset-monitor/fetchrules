// Copyright 2024 Google LLC
//
// Licensed under the Apache License, Version 2.0 (the 'License');
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an 'AS IS' BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//.

package fetchrules

import (
	"bytes"
	"fmt"
	"log"
	"os"
	"path/filepath"
	"strings"
	"testing"
)

func TestUnitGetConstraint(t *testing.T) {
	var testCases = []struct {
		name            string
		path            string
		want            string
		wantMsgContains string
		countChanges    int
	}{
		{
			name:         "assetType",
			path:         "testdata/rules01/bq_dataset_location/bq_dataset_location_europe.yaml",
			want:         "bigquery.googleapis.com/Dataset",
			countChanges: 1,
		},
		{
			name:         "contentType",
			path:         "testdata/rules01/iam/bindings_roles/iam_no_billing_account_creator/iam_no_billing_account_creator.yaml",
			want:         "IAM_POLICY",
			countChanges: 1,
		},
		{
			name:            "no_valid_format",
			path:            "testdata/any.yaml",
			wantMsgContains: "ignored not a valid constraint format",
			countChanges:    0,
		},
	}
	for _, tc := range testCases {
		tc := tc // https://github.com/golang/go/wiki/CommonMistakes#using-goroutines-on-loop-iterator-variables
		t.Run(tc.name, func(t *testing.T) {
			// t.Parallel()
			p := filepath.Clean(tc.path)
			if !strings.HasPrefix(p, "testdata/") {
				panic(fmt.Errorf("Unsafe path %s", p)) // prevent G304
			}
			b, err := os.ReadFile(p)
			if err != nil {
				log.Fatalln(err)
			}
			var constraints []constraint
			var buffer bytes.Buffer
			log.SetOutput(&buffer)
			defer func() {
				log.SetOutput(os.Stderr)
			}()
			constraints = getConstraint(p, b, constraints)
			msgString := buffer.String()

			if tc.want != "" {
				if constraints[0].contentOrAssetType != tc.want {
					t.Errorf("Want content or asset type %s got %s", tc.want, constraints[0].contentOrAssetType)
				}
			}
			if !strings.Contains(msgString, tc.wantMsgContains) {
				t.Errorf("want msg to contains '%s' and got \n'%s'", tc.wantMsgContains, msgString)
			}
			if len(constraints) != tc.countChanges {
				t.Errorf("want %d changes in constraints map and got %d", tc.countChanges, len(constraints))
			}
		})
	}
}
