// Copyright 2024 Google LLC
//
// Licensed under the Apache License, Version 2.0 (the 'License');
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an 'AS IS' BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//

package fetchrules

import (
	"bytes"
	"encoding/json"
	"fmt"
	"log"
	"os"
	"path/filepath"
	"strings"
	"testing"
	"time"

	"cloud.google.com/go/pubsub"
	"gitlab.com/realtime-asset-monitor/utilities/glo"
)

func TestIntegEntryPoint(t *testing.T) {
	testCases := []struct {
		name            string
		messageType     string
		origin          string
		assetType       string
		contentType     string
		missPublishTime bool
		missOrigin      bool
		missContentType bool
		feedPath        string
		rulesFolderPath string
		wantMsgContains []string
		wantErr         bool
	}{
		{
			name:            "missing_time",
			feedPath:        "testdata/asset_feed_01.json",
			assetType:       "cloudresourcemanager.googleapis.com/Project",
			missPublishTime: true,
			wantMsgContains: []string{
				"noretry",
				"triggering PubSub message attribute 'timestamp' is missing or invalid and should not",
			},
		},
		{
			name:        "wrong message-type",
			feedPath:    "testdata/asset_feed_01.json",
			assetType:   "cloudresourcemanager.googleapis.com/Project",
			messageType: "blabla",
			wantMsgContains: []string{
				"noretry",
				"want triggering PubSub message attribute 'messageType' to be 'asset_feed' and got",
			},
		},
		{
			name:            "missing assetType",
			feedPath:        "testdata/asset_feed_01.json",
			rulesFolderPath: "testdata/rules99",
			wantMsgContains: []string{
				"triggering PubSub message attribute 'assetType' is zero value and should not",
			},
		},
		{
			name:       "missing origin",
			missOrigin: true,
			feedPath:   "testdata/asset_feed_01.json",
			assetType:  "cloudresourcemanager.googleapis.com/Project",
			wantMsgContains: []string{
				"noretry",
				"triggering PubSub message attribute 'origin' is zero value and should not",
			},
		},
		{
			name:            "missing contentType",
			missContentType: true,
			feedPath:        "testdata/asset_feed_01.json",
			assetType:       "cloudresourcemanager.googleapis.com/Project",
			rulesFolderPath: "testdata/rules99",
			wantMsgContains: []string{
				"triggering PubSub message attribute 'contentType' is zero value and should not",
			},
		},
		{
			name:        "feed_iam_policy",
			feedPath:    "testdata/asset_feed_02.json",
			assetType:   "cloudresourcemanager.googleapis.com/Project",
			contentType: "IAM_POLICY",
			wantMsgContains: []string{
				"cloud-events published count 2",
			},
		},
		{
			name:            "feed_resource",
			feedPath:        "testdata/asset_feed_01.json",
			assetType:       "cloudresourcemanager.googleapis.com/Project",
			rulesFolderPath: "testdata/rules99",
			wantMsgContains: []string{
				"cloud-events published count 2",
			},
		},
		{
			name:            "feed_unknown",
			feedPath:        "testdata/asset_feed_03.json",
			assetType:       "cloudresourcemanager.googleapis.com/Project",
			rulesFolderPath: "testdata/rules99",
			wantMsgContains: []string{
				"unmanaged contentOrAssetType",
			},
		},
	}
	projectID := os.Getenv("FETCHRULES_PROJECT_ID") //
	if projectID == "" {
		t.Skip("Missing env var FETCHRULES_PROJECT_ID")
	}
	rulesRepoBucketName := projectID + "-rulesrepo"

	now := time.Now()

	global.serviceEnv.CacheMaxAgeMinutes = 0

	for _, tc := range testCases {
		tc := tc // prevent G601
		t.Run(tc.name, func(t *testing.T) {
			var attr map[string]string
			var pubsubMsg pubsub.Message
			attr = make(map[string]string)

			if !tc.missPublishTime {
				attr["timestamp"] = now.Format(time.RFC3339)
			}
			if tc.messageType == "" {
				attr["messageType"] = "asset_feed"
			} else {
				attr["messageType"] = tc.messageType
			}
			attr["assetType"] = tc.assetType
			attr["contentType"] = tc.contentType
			if tc.missContentType {
				attr["contentType"] = ""
			} else {
				if tc.messageType == "" {
					attr["contentType"] = "RESOURCE"
				} else {
					attr["contentType"] = tc.contentType
				}
			}
			if tc.missOrigin {
				attr["origin"] = ""
			} else {
				if tc.origin == "" {
					attr["origin"] = "real-time"
				} else {
					attr["origin"] = tc.origin
				}
			}
			attr["microserviceName"] = "convertfeed"

			pubsubMsg.Attributes = attr
			pubsubMsg.ID = "c56c9245-0af7-44c7-8a55-954583940e09"

			p := filepath.Clean(tc.feedPath)
			if !strings.HasPrefix(p, "testdata/") {
				panic(fmt.Errorf("Unsafe path %s", p)) // prevent G304
			}
			b, err := os.ReadFile(p)
			if err != nil {
				log.Fatalln(err)
			}

			pubsubMsg.Data = b

			if tc.rulesFolderPath != "" {
				err := DeployRules(ctx,
					tc.rulesFolderPath,
					"",
					rulesRepoBucketName,
					projectID,
					"",
					false,
					"")
				if err != nil {
					log.Fatalln(err)
				}
			}

			var buffer bytes.Buffer
			log.SetOutput(&buffer)
			defer func() {
				log.SetOutput(os.Stderr)
			}()
			err = EntryPoint(ctx, pubsubMsg)
			if err != nil {
				if !tc.wantErr {
					t.Errorf("want no error and got %v", err)
				}
			}
			msgString := buffer.String()
			// t.Logf("%v", msgString)
			for _, wantMsg := range tc.wantMsgContains {
				if !strings.Contains(msgString, wantMsg) {
					t.Errorf("want msg to contains: %s", tc.wantMsgContains)
				}
			}
			logEntriesString := strings.Split(msgString, "\n")
			for _, logEntryString := range logEntriesString {
				var logEntry glo.Entry
				err = json.Unmarshal([]byte(logEntryString), &logEntry)
				if !(err != nil) {
					if logEntry.MicroserviceName == "" {
						t.Errorf("logEntry.MicroserviceName is null should not %s", logEntry.Message)
					}
					if logEntry.Environment == "" {
						t.Errorf("logEntry.Environment is null should not %s", logEntry.Message)
					}
					if strings.HasPrefix(strings.ToLower(logEntry.Message), "finish") {
						if len(logEntry.StepStack) == 0 {
							t.Errorf("logEntry.StepStack len is zero and should not %s", logEntry.Message)
						}
						if logEntry.LatencyE2ESeconds == 0 {
							t.Errorf("logEntry.LatencyE2ESeconds is zero and should not %s", logEntry.Message)
						}
						if logEntry.LatencySeconds == 0 {
							t.Errorf("logEntry.LatencySeconds is zero and should not %s", logEntry.Message)
						}
					}
				}
			}
		})
	}
}
