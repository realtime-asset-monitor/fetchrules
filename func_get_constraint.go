// Copyright 2024 Google LLC
//
// Licensed under the Apache License, Version 2.0 (the 'License');
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an 'AS IS' BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//.

package fetchrules

import (
	"fmt"
	"log"

	"gitlab.com/realtime-asset-monitor/utilities/cai"
	"gopkg.in/yaml.v2"
)

func getConstraint(path string, b []byte, constraints []constraint) []constraint {
	var c constraint
	var cc cai.ConstraintConfig
	var contentType, assetType, contentOrAssetType string
	err := yaml.Unmarshal(b, &cc)
	if err != nil {
		log.Printf("ignored not a valid constraint format %s %v", path, err)
	} else {
		if cc.Metadata.Name == "" {
			log.Printf("ignored no metadata:name %s", path)
		} else {
			if cc.Kind == "" {
				log.Printf("ignored no kind %s", path)
			} else {
				contentType = getStringValue(cc.Metadata.Annotations["contentType"])
				assetType = getStringValue(cc.Metadata.Annotations["assetType"])
				if contentType != "" {
					if assetType != "" {
						// both content type and asset type means the rule scope is narrowed to this combination
						contentOrAssetType = fmt.Sprintf("%s%s", assetType, contentType)
					} else {
						// only content type means the rule applies to any asset type, for this content type
						contentOrAssetType = contentType
					}
				} else {
					// no content type means implicitly the content type is RESOURCE
					contentOrAssetType = assetType
				}
				if contentOrAssetType == "" {
					log.Printf("ignored no metadata:annotations:contentType nor metadata:annotations:assetType %s", path)
				} else {
					c.assetType = assetType
					c.contentType = contentType
					c.constraintConfig = cc
					c.contentOrAssetType = contentOrAssetType
					constraints = append(constraints, c)
				}
			}
		}
	}
	return constraints
}
