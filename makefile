SERVICE_NAME=fetchrules

all: test run

test: goupd gotest sast

goupd:
	go get -u -t ./...
	go mod tidy

gotest:
	go version
	go test ./... -coverprofile=coverage.out
	go tool cover -html=coverage.out -o coverage.html
	go tool cover -func=coverage.out

sast:
	gosec ./...

run:
	go run ./cmd/main.go

packbuild:
	pack build --builder gcr.io/buildpacks/builder:google-22 \
	--env GOOGLE_FUNCTION_SIGNATURE_TYPE=cloudevent \
	--env GOOGLE_FUNCTION_TARGET=EntryPoint \
	${SERVICE_NAME}
	docker images

dockerrun:
	PORT=8080 && docker run \
	-p 8081:${PORT} \
	-e PORT=${PORT} \
	-e K_SERVICE=dockerrun \
	-e K_CONFIGURATION=dockerrun \
	-e K_REVISION=dockerrun-$(git rev-parse HEAD) \
	-e FETCHRULES_PROJECT_ID=${FETCHRULES_PROJECT_ID} \
	-e GOOGLE_APPLICATION_CREDENTIALS=/tmp/keys/key.json \
	-v $$GOOGLE_APPLICATION_CREDENTIALS:/tmp/keys/key.json:ro \
	${SERVICE_NAME}
