// Copyright 2024 Google LLC
//
// Licensed under the Apache License, Version 2.0 (the 'License');
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an 'AS IS' BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//

package fetchrules

import (
	"time"

	"cloud.google.com/go/storage"
	cloudevents "github.com/cloudevents/sdk-go/v2"
	cmap "github.com/orcaman/concurrent-map"
	"gitlab.com/realtime-asset-monitor/utilities/cai"
	"gitlab.com/realtime-asset-monitor/utilities/glo"
)

const microserviceName = "fetchrules"
const outputEventType = "asset_rule"

// ServiceEnv list environment variables prefixed with the name of the microservice
type ServiceEnv struct {
	AssetRuleTopicID      string  `envconfig:"asset_rule_topic_id" default:"assetRule"`
	CacheMaxAgeMinutes    float64 `envconfig:"cache_max_age_minutes" default:"60"`
	CommonRegoBucketName  string  `envconfig:"common_rego_bucket_name"`
	Environment           string  `envconfig:"environment" default:"dev"`
	LogOnlySeveritylevels string  `envconfig:"log_only_severity_levels" default:"WARNING NOTICE CRITICAL"`
	ProjectID             string  `envconfig:"project_id" required:"true"`
	RulesRepoBucketName   string  `envconfig:"rules_repo_bucket_name"`
	StartProfiler         bool    `envconfig:"start_profiler" default:"false"`
}

// Env list environment variables
type Env struct {
	KConfiguration string `envconfig:"k_configuration"`
	KRevision      string `envconfig:"k_revision"`
	KService       string `envconfig:"k_service"`
}

type constraint struct {
	assetType          string
	contentType        string
	contentOrAssetType string
	constraintConfig   cai.ConstraintConfig
}

// ConstraintConfigs map key is the constraint name
type ConstraintConfigs map[string]cai.ConstraintConfig

// RuleConfig one rego rule and associated constraint configs
type RuleConfig struct {
	RegoCode          string            `json:"regoCode"`
	Version           time.Time         `json:"version"`
	ConstraintConfigs ConstraintConfigs `json:"constraintConfigs"`
}

// RuleConfigs map key is the rule name aka the kind
type RuleConfigs map[string]*RuleConfig

// ContentOrAssetTypeConfigs map key is the ContentType or the AssetType
type ContentOrAssetTypeConfigs map[string]RuleConfigs

type readmeSet map[string]string
type regoRules map[string]string

// RulesRepo repository of rules by ContentOrAssetType / rule kind / constraint name
type RulesRepo struct {
	Version                   time.Time                 `json:"version"`
	ContentOrAssetTypeConfigs ContentOrAssetTypeConfigs `json:"contentOrAssetTypeConfigs"`
}

// contentOrAssetTypeConfig is the structure used in the cache cmap
type contentOrAssetTypeConfig struct {
	version     time.Time
	foundInGCS  bool
	ruleConfigs RuleConfigs
}

// Global structure for global variables to optimize the performances in serverless mode
type Global struct {
	assetRuleClient                cloudevents.Client
	cacheContentOrAssetTypeConfigs cmap.ConcurrentMap
	CommonEv                       glo.CommonEntryValues
	commonRego                     map[string]string
	env                            *Env
	// ruleRepo                       *RulesRepo // to be removed
	rulesRepoBucket *storage.BucketHandle
	serviceEnv      *ServiceEnv
}
