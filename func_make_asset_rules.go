// Copyright 2024 Google LLC
//
// Licensed under the Apache License, Version 2.0 (the 'License');
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an 'AS IS' BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//.

package fetchrules

import (
	"context"
	"fmt"

	"cloud.google.com/go/storage"
	"gitlab.com/realtime-asset-monitor/utilities/cai"
)

func makeAssetRules(ctx context.Context,
	caiFeedMsg *cai.FeedMessage,
	rulesRepoBucket *storage.BucketHandle) (contentOrAssetTypes []string, assetRules []cai.AssetRule, err error) {
	assetRules = make([]cai.AssetRule, 0)
	contentOrAssetTypes = make([]string, 0)
	if len(string(caiFeedMsg.Asset.IamPolicy)) == 0 || string(caiFeedMsg.Asset.IamPolicy) == "null" {
		//  get implicit RESOURCE rules for this asset type
		r, err := getAssetRules(ctx, caiFeedMsg.Asset.AssetType, caiFeedMsg, rulesRepoBucket)
		if err != nil {
			return contentOrAssetTypes, assetRules, err
		}
		if len(r) != 0 {
			assetRules = append(assetRules, r...)
			contentOrAssetTypes = append(contentOrAssetTypes, caiFeedMsg.Asset.AssetType)
		}
		// get explicit RESOURCE rules for this asset type
		c := fmt.Sprintf("%sRESOURCE", caiFeedMsg.Asset.AssetType)
		r, err = getAssetRules(ctx, c, caiFeedMsg, rulesRepoBucket)
		if err != nil {
			return contentOrAssetTypes, assetRules, err
		}
		if len(r) != 0 {
			assetRules = append(assetRules, r...)
			contentOrAssetTypes = append(contentOrAssetTypes, c)
		}
	} else {
		//  Get IAM rules that apply to all assetTypes
		c := "IAM_POLICY"
		r, err := getAssetRules(ctx, c, caiFeedMsg, rulesRepoBucket)
		if err != nil {
			return contentOrAssetTypes, assetRules, err
		}
		if len(r) != 0 {
			assetRules = append(assetRules, r...)
			contentOrAssetTypes = append(contentOrAssetTypes, c)
		}
		// Get IAM rules specific to this asset type
		c = fmt.Sprintf("%sIAM_POLICY", caiFeedMsg.Asset.AssetType)
		r, err = getAssetRules(ctx, c, caiFeedMsg, rulesRepoBucket)
		if err != nil {
			return contentOrAssetTypes, assetRules, err
		}
		if len(r) != 0 {
			assetRules = append(assetRules, r...)
			contentOrAssetTypes = append(contentOrAssetTypes, c)
		}
	}
	return contentOrAssetTypes, assetRules, err
}
