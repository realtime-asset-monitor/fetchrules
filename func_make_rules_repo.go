// Copyright 2024 Google LLC
//
// Licensed under the Apache License, Version 2.0 (the 'License');
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an 'AS IS' BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//.

package fetchrules

import (
	"log"
	"sort"
	"time"
)

func makeRulesRepo(onlyType string, constraints []constraint, rules regoRules, readmes readmeSet) *RulesRepo {
	var repo RulesRepo
	repo.ContentOrAssetTypeConfigs = make(ContentOrAssetTypeConfigs)

	sort.Slice(constraints, func(i, j int) bool {
		return constraints[i].contentOrAssetType > constraints[j].contentOrAssetType
	})
	// ensure all deployed rules at the same time share the same version timestamp to enable linked asset rules
	v := time.Now()
	for _, constraint := range constraints {
		constraint := constraint
		code := rules[constraint.constraintConfig.Kind]
		readme := readmes[constraint.constraintConfig.Metadata.Name]
		if code == "" {
			log.Printf("missing rule %s used in constraint %s", constraint.constraintConfig.Kind, constraint.constraintConfig.Metadata.Name)
		} else {
			t := constraint.contentOrAssetType
			if onlyType == t || onlyType == "" {
				k := constraint.constraintConfig.Kind
				n := constraint.constraintConfig.Metadata.Name
				c := constraint.constraintConfig
				if readme != "" {
					c.Metadata.Annotations["readme"] = readme
				}
				c.Metadata.Annotations["version"] = v
				_, ok := repo.ContentOrAssetTypeConfigs[t]
				if !ok {
					repo.ContentOrAssetTypeConfigs[t] = make(RuleConfigs)
				}
				cConfigs := make(ConstraintConfigs)
				cConfigs[n] = constraint.constraintConfig
				_, ok = repo.ContentOrAssetTypeConfigs[t][k]
				if !ok {
					var rc RuleConfig
					rc.RegoCode = code
					rc.Version = v
					rc.ConstraintConfigs = make(ConstraintConfigs)
					rc.ConstraintConfigs[n] = c
					repo.ContentOrAssetTypeConfigs[t][k] = &rc
				} else {
					repo.ContentOrAssetTypeConfigs[t][k].RegoCode = code
					repo.ContentOrAssetTypeConfigs[t][k].Version = v
					repo.ContentOrAssetTypeConfigs[t][k].ConstraintConfigs[n] = c
				}
			}
		}
	}
	repo.Version = time.Now()
	return &repo
}
