// Copyright 2024 Google LLC
//
// Licensed under the Apache License, Version 2.0 (the 'License');
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an 'AS IS' BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//.

package fetchrules

import (
	"log"
	"strings"
)

func getRule(path string, b []byte, rules regoRules) regoRules {
	lines := strings.Split(string(b), "\n")
	found := false
	kind := ""
	for _, line := range lines {
		line := line // prevent G601
		if strings.Contains(line, "package templates.gcp.") {
			found = true
			kind = strings.Trim(strings.TrimPrefix(line, "package templates.gcp."), " ")
			break
		}
	}
	if !found {
		log.Printf("ignored no kind %s", path)
	} else {
		rules[kind] = string(b)
	}
	return rules
}
