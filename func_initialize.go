// Copyright 2024 Google LLC
//
// Licensed under the Apache License, Version 2.0 (the 'License');
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an 'AS IS' BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//

package fetchrules

import (
	"context"
	"fmt"
	"io"
	"log"
	"strings"
	"time"

	"cloud.google.com/go/profiler"
	"cloud.google.com/go/storage"
	cepubsub "github.com/cloudevents/sdk-go/protocol/pubsub/v2"
	cloudevents "github.com/cloudevents/sdk-go/v2"
	"github.com/google/uuid"
	"github.com/kelseyhightower/envconfig"
	cmap "github.com/orcaman/concurrent-map"
	"gitlab.com/realtime-asset-monitor/utilities/glo"
	"google.golang.org/api/iterator"
)

var global Global
var ctx = context.Background()

// initialize is called by init() and enable tests
func initialize() {
	start := time.Now()
	log.SetFlags(0)
	global.CommonEv.InitID = fmt.Sprintf("%v", uuid.New())
	var err error
	var serviceEnv ServiceEnv

	err = envconfig.Process(microserviceName, &serviceEnv)
	if err != nil {
		log.Println(glo.Entry{
			MicroserviceName: microserviceName,
			Severity:         "CRITICAL",
			Message:          "init_failed",
			Description:      fmt.Sprintf("envconfig.Process %s %v", microserviceName, err),
			InitID:           global.CommonEv.InitID,
		})
		log.Fatalf("INIT_FAILURE %v", err)
	}
	global.serviceEnv = &serviceEnv
	global.CommonEv.MicroserviceName = microserviceName
	global.CommonEv.Environment = global.serviceEnv.Environment
	global.CommonEv.LogOnlySeveritylevels = global.serviceEnv.LogOnlySeveritylevels
	var ev glo.EntryValues
	ev.CommonEntryValues = global.CommonEv
	glo.LogInitColdStart(ev)

	if global.serviceEnv.RulesRepoBucketName == "" {
		global.serviceEnv.RulesRepoBucketName = global.serviceEnv.ProjectID + "-rulesrepo"
	}

	if global.serviceEnv.CommonRegoBucketName == "" {
		global.serviceEnv.CommonRegoBucketName = global.serviceEnv.ProjectID + "-commonrego"
	}

	var env Env
	err = envconfig.Process("", &env)
	if err != nil {
		glo.LogInitFatal(ev, "envconfig.Process no prefix", err)
	}
	global.env = &env

	if serviceEnv.StartProfiler {
		err := profiler.Start(profiler.Config{
			ProjectID:            serviceEnv.ProjectID,
			Service:              microserviceName,
			ServiceVersion:       env.KRevision,
			DebugLogging:         false,
			NoGoroutineProfiling: true,
			NoAllocProfiling:     true,
		})
		if err != nil {
			glo.LogInitFatal(ev, "failed to start the profiler", err)
		}
	}

	assetRuleTransport, err := cepubsub.New(ctx,
		cepubsub.WithProjectID(global.serviceEnv.ProjectID),
		cepubsub.WithTopicID(global.serviceEnv.AssetRuleTopicID))
	if err != nil {
		glo.LogInitFatal(ev, "failed to start the profiler", err)
	}

	global.assetRuleClient, err = cloudevents.NewClient(assetRuleTransport, cloudevents.WithTimeNow(), cloudevents.WithUUIDs())
	if err != nil {
		glo.LogInitFatal(ev, "cloudevents.NewClient", err)
	}

	storageClient, err := storage.NewClient(ctx)
	if err != nil {
		glo.LogInitFatal(ev, "storage.NewClient", err)
	}
	global.rulesRepoBucket = storageClient.Bucket(global.serviceEnv.RulesRepoBucketName)
	commonRegoBucket := storageClient.Bucket(global.serviceEnv.CommonRegoBucketName)
	query := &storage.Query{Prefix: ""}
	it := commonRegoBucket.Objects(ctx, query)
	global.commonRego = make(map[string]string)
	for {
		attrs, err := it.Next()
		if err == iterator.Done {
			break
		}
		if err != nil {
			glo.LogInitFatal(ev, "commonRegoBucket.Objects", err)
		}
		// fmt.Println(attrs.Name)
		n := strings.ToLower(attrs.Name)
		if strings.HasSuffix(n, ".rego") {
			reader, err := commonRegoBucket.Object(attrs.Name).NewReader(ctx)
			if err != nil {
				glo.LogInitFatal(ev, "commonRegoBucket.Object.NewReader", err)
			}
			defer reader.Close()
			b, err := io.ReadAll(reader)
			if err != nil {
				glo.LogInitFatal(ev, "io.ReadAll", err)
			}
			global.commonRego[n] = string(b)
		}
	}

	global.cacheContentOrAssetTypeConfigs = cmap.New()

	glo.LogInitDone(ev, time.Since(start).Seconds())
}
