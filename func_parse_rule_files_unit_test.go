// Copyright 2024 Google LLC
//
// Licensed under the Apache License, Version 2.0 (the 'License');
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an 'AS IS' BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//.

package fetchrules

import (
	"testing"
)

func TestUnitParseRuleFiles(t *testing.T) {
	var testCases = []struct {
		name             string
		path             string
		countConstraints int
		countRules       int
		countReadmes     int
	}{
		{
			name:             "rules01",
			path:             "testdata/rules01",
			countConstraints: 12,
			countRules:       7,
			countReadmes:     12,
		},
	}
	for _, tc := range testCases {
		tc := tc // https://github.com/golang/go/wiki/CommonMistakes#using-goroutines-on-loop-iterator-variables
		t.Run(tc.name, func(t *testing.T) {
			t.Parallel()
			constraints, rules, readmes, err := parseRuleFiles(tc.path)
			if err != nil {
				t.Errorf("want no error, got %v", err)
			}
			if len(constraints) != tc.countConstraints {
				t.Errorf("want %d constraints got %d", tc.countConstraints, len(constraints))
			}
			if len(rules) != tc.countRules {
				t.Errorf("want %d rules got %d", tc.countRules, len(rules))
			}
			if len(readmes) != tc.countReadmes {
				t.Errorf("want %d readme got %d", tc.countReadmes, len(readmes))
			}
		})
	}
}
