// Copyright 2024 Google LLC
//
// Licensed under the Apache License, Version 2.0 (the 'License');
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an 'AS IS' BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//.

package fetchrules

import (
	"bytes"
	"fmt"
	"log"
	"os"
	"path/filepath"
	"strings"
	"testing"
)

func TestUnitGetRule(t *testing.T) {
	var testCases = []struct {
		name            string
		path            string
		want            string
		wantMsgContains string
		countChanges    int
	}{
		{
			name:         "GCPBigQueryDatasetLocationConstraintV1",
			path:         "testdata/rules01/bq_dataset_location/GCPBigQueryDatasetLocationConstraintV1.rego",
			want:         "GCPBigQueryDatasetLocationConstraintV1",
			countChanges: 1,
		},
		{
			name:            "fake_rego",
			path:            "testdata/fake.rego",
			wantMsgContains: "ignored no kind",
			countChanges:    0,
		},
	}
	for _, tc := range testCases {
		tc := tc // https://github.com/golang/go/wiki/CommonMistakes#using-goroutines-on-loop-iterator-variables
		t.Run(tc.name, func(t *testing.T) {
			p := filepath.Clean(tc.path)
			if !strings.HasPrefix(p, "testdata/") {
				panic(fmt.Errorf("Unsafe path %s", p)) // prevent G304
			}
			b, err := os.ReadFile(p)
			if err != nil {
				log.Fatalln(err)
			}
			regoRules := make(regoRules)
			var buffer bytes.Buffer
			log.SetOutput(&buffer)
			defer func() {
				log.SetOutput(os.Stderr)
			}()
			regoRules = getRule(p, b, regoRules)
			msgString := buffer.String()

			if tc.want != "" {
				if _, ok := regoRules[tc.want]; !ok {
					t.Errorf("Want %s in rego rules map and did not get it", tc.want)
				}
			}
			if !strings.Contains(msgString, tc.wantMsgContains) {
				t.Errorf("want msg to contains '%s' and got \n'%s'", tc.wantMsgContains, msgString)
			}
			if len(regoRules) != tc.countChanges {
				t.Errorf("want %d changes in readmes map and got %d", tc.countChanges, len(regoRules))
			}
		})
	}
}
