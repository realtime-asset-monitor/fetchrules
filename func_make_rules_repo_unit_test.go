// Copyright 2024 Google LLC
//
// Licensed under the Apache License, Version 2.0 (the 'License');
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an 'AS IS' BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//.

package fetchrules

import (
	"bytes"
	"log"
	"os"
	"strings"
	"testing"
)

func TestUnitMakeRulesRepo(t *testing.T) {
	var testCases = []struct {
		name            string
		path            string
		onlyType        string
		countTypes      int
		wantMsgContains string
		countRuleConfig int
		countConstraint int
	}{
		{
			name:            "rules01",
			path:            "testdata/rules01",
			countTypes:      3,
			countRuleConfig: 7,
			countConstraint: 12,
		},
		{
			name:            "rules02",
			path:            "testdata/rules02",
			countTypes:      0,
			wantMsgContains: "missing rule",
			countRuleConfig: 0,
			countConstraint: 0,
		},
		{
			name:            "only_IAM_POLICY",
			path:            "testdata/rules01",
			onlyType:        "IAM_POLICY",
			countTypes:      1,
			countRuleConfig: 2,
			countConstraint: 7,
		},
		{
			name:            "only_bigquery.googleapis.com/Dataset",
			path:            "testdata/rules01",
			onlyType:        "bigquery.googleapis.com/Dataset",
			countTypes:      1,
			countRuleConfig: 1,
			countConstraint: 1,
		},
		{
			name:            "only_bigquery.googleapis.com/Dataset",
			path:            "testdata/rules01",
			onlyType:        "cloudfunctions.googleapis.com/CloudFunction",
			countTypes:      1,
			countRuleConfig: 4,
			countConstraint: 4,
		},
	}
	for _, tc := range testCases {
		tc := tc // https://github.com/golang/go/wiki/CommonMistakes#using-goroutines-on-loop-iterator-variables
		t.Run(tc.name, func(t *testing.T) {
			t.Parallel()
			constraints, rules, readmes, err := parseRuleFiles(tc.path)
			if err != nil {
				t.Fatalf("parseRuleFiles(tc.path) %v", err)
			}
			var buffer bytes.Buffer
			log.SetOutput(&buffer)
			defer func() {
				log.SetOutput(os.Stderr)
			}()
			rr := makeRulesRepo(tc.onlyType, constraints, rules, readmes)
			msgString := buffer.String()
			if !strings.Contains(msgString, tc.wantMsgContains) {
				t.Errorf("want msg to contains '%s' and got \n'%s'", tc.wantMsgContains, msgString)
			}

			if len(rr.ContentOrAssetTypeConfigs) != tc.countTypes {
				t.Errorf("want %d content or asset types got %d", tc.countTypes, len(rr.ContentOrAssetTypeConfigs))
			}
			countRuleConfig := 0
			countConstraint := 0
			for contentOrAssetType, ruleConfigs := range rr.ContentOrAssetTypeConfigs {
				for kind, ruleConfig := range ruleConfigs {
					countRuleConfig++
					if ruleConfig.Version.String() == "" {
						t.Errorf("want timestamp as version and got none %s %s", contentOrAssetType, kind)
					}
					if ruleConfig.RegoCode == "" {
						t.Errorf("want no rego code and want some %s %s", contentOrAssetType, kind)
					}
					for constraintName, constraintConfig := range ruleConfig.ConstraintConfigs {
						countConstraint++
						if constraintName != constraintConfig.Metadata.Name {
							t.Errorf("name in map key %s is different from metadata %s in %s %s", constraintName, constraintConfig.Metadata.Name, contentOrAssetType, kind)
						}
						if kind != constraintConfig.Kind {
							t.Errorf("Find kind %s in constraint %s and wants %s %s", constraintConfig.Kind, constraintName, kind, contentOrAssetType)
						}
					}
				}
			}
			if countRuleConfig != tc.countRuleConfig {
				t.Errorf("want %d rules got %d", tc.countRuleConfig, countRuleConfig)
			}
			if countConstraint != tc.countConstraint {
				t.Errorf("want %d constraints got %d", tc.countConstraint, countConstraint)
			}
		})
	}

}
