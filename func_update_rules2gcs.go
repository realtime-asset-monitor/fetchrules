// Copyright 2024 Google LLC
//
// Licensed under the Apache License, Version 2.0 (the 'License');
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an 'AS IS' BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//.

package fetchrules

import (
	"context"
	"fmt"
	"log"
	"sort"
	"strings"

	"cloud.google.com/go/storage"
	jsoniter "github.com/json-iterator/go"
	"gitlab.com/realtime-asset-monitor/utilities/glo"
	"google.golang.org/api/iterator"
)

func updateRules2gcs(ctx context.Context, onlyType string, rulesRepo *RulesRepo,
	rulesRepoBucket *storage.BucketHandle,
	logOnlySeveritylevels string) (err error) {

	contentOrAssetTypes := make([]string, 0, len(rulesRepo.ContentOrAssetTypeConfigs))
	for contentOrAssetType := range rulesRepo.ContentOrAssetTypeConfigs {
		contentOrAssetTypes = append(contentOrAssetTypes, contentOrAssetType)
	}
	sort.Strings(contentOrAssetTypes)
	for _, contentOrAssetType := range contentOrAssetTypes {
		ruleConfigs := rulesRepo.ContentOrAssetTypeConfigs[contentOrAssetType]
		b, err := jsoniter.MarshalIndent(ruleConfigs, "", "  ")
		if err != nil {
			return err
		}

		storageObject := rulesRepoBucket.Object(contentOrAssetType)
		storageObjectWriter := storageObject.NewWriter(ctx)
		_, err = fmt.Fprint(storageObjectWriter, string(b))
		if err != nil {
			return err
		}
		err = storageObjectWriter.Close()
		if err != nil {
			return err
		}

		if strings.Contains(logOnlySeveritylevels, "INFO") {
			log.Println(glo.Entry{
				MicroserviceName: microserviceName,
				Environment:      global.serviceEnv.Environment,
				Severity:         "INFO",
				Message:          fmt.Sprintf("rules repo updated %s", contentOrAssetType),
			})
		}
	}
	if onlyType == "" {
		query := &storage.Query{Prefix: ""}
		it := rulesRepoBucket.Objects(ctx, query)
		for {
			attrs, err := it.Next()
			if err == iterator.Done {
				break
			}
			if err != nil {
				return fmt.Errorf("it.Next() %v", err)
			}
			if _, ok := rulesRepo.ContentOrAssetTypeConfigs[attrs.Name]; !ok {
				err = rulesRepoBucket.Object(attrs.Name).Delete(ctx)
				if err != nil {
					return fmt.Errorf("rulesRepoBucket.Object(attrs.Name).Delete(ctx) %s %v", attrs.Name, err)
				}
				if strings.Contains(logOnlySeveritylevels, "INFO") {
					log.Println(glo.Entry{
						MicroserviceName: microserviceName,
						Environment:      global.serviceEnv.Environment,
						Severity:         "INFO",
						Message:          fmt.Sprintf("content or asset type deleted from the rules repo %s", attrs.Name),
					})
				}
			}
		}
	}

	return nil
}
