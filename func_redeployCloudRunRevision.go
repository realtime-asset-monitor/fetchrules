// Copyright 2024 Google LLC
//
// Licensed under the Apache License, Version 2.0 (the 'License');
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an 'AS IS' BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//.

package fetchrules

import (
	"context"
	"encoding/json"
	"fmt"
	"strings"
	"time"

	run "cloud.google.com/go/run/apiv2"
	runpb "cloud.google.com/go/run/apiv2/runpb"
)

func redeployCloudRunRevision(ctx context.Context, projectID string, location string) (cRunSrv *runpb.Service, err error) {
	srvCli, err := run.NewServicesClient(ctx)
	if err != nil {
		return cRunSrv, err
	}
	defer srvCli.Close()

	srvGetReq := &runpb.GetServiceRequest{
		Name: fmt.Sprintf("projects/%v/locations/%s/services/%s", projectID, location, microserviceName),
	}

	cRunSrv, err = srvCli.GetService(ctx, srvGetReq)
	if err != nil {
		return cRunSrv, err
	}
	fmt.Printf("retrieved service %s uid %s\n", cRunSrv.Name, cRunSrv.Uid)
	fmt.Printf("current revision %s\ntraffic\n", cRunSrv.Template.Revision)
	by, err := json.MarshalIndent(cRunSrv.Traffic, "", "    ")
	if err != nil {
		return cRunSrv, err
	}
	fmt.Printf("%s\n", string(by))

	// by, err := json.MarshalIndent(cRunSrv, "", "    ")
	// if err != nil {
	// 	return cRunSrv, err
	// }
	// fmt.Printf("%s\n", string(by))

	now := time.Now()
	cRunSrv.Template.Revision = fmt.Sprintf("%s-ramcli-%s", microserviceName, strings.ToLower(strings.ReplaceAll(strings.ReplaceAll(now.Format(time.RFC3339), ":", "-"), "+", "plus")))

	var trafficTarget runpb.TrafficTarget
	trafficTarget.Percent = 100
	trafficTarget.Type = runpb.TrafficTargetAllocationType_TRAFFIC_TARGET_ALLOCATION_TYPE_LATEST

	traffic := make([]*runpb.TrafficTarget, 0)
	traffic = append(traffic, &trafficTarget)
	cRunSrv.Traffic = traffic

	srvUpdReq := &runpb.UpdateServiceRequest{
		Service: cRunSrv,
	}

	op, err := srvCli.UpdateService(ctx, srvUpdReq)
	if err != nil {
		return cRunSrv, err
	}
	fmt.Println("on going new revision deployment, tic tac...")
	srvUpdOps, err := op.Wait(ctx)
	if err != nil {
		return cRunSrv, err
	}
	cRunSrv = srvUpdOps
	fmt.Printf("new revision %s\ntraffic\n", cRunSrv.Template.Revision)
	by, err = json.MarshalIndent(cRunSrv.Traffic, "", "    ")
	if err != nil {
		return cRunSrv, err
	}
	fmt.Printf("%s\n", string(by))

	// fmt.Println("\nall revisions")
	// revCli, err := run.NewRevisionsClient(ctx)
	// if err != nil {
	// 	return cRunSrv, err
	// }
	// defer revCli.Close()

	// revListReq := &runpb.ListRevisionsRequest{
	// 	Parent: fmt.Sprintf("projects/%v/locations/%s/services/%s", projectID, location, microserviceName),
	// }
	// it := revCli.ListRevisions(ctx, revListReq)
	// for {
	// 	rev, err := it.Next()
	// 	if err == iterator.Done {
	// 		break
	// 	}
	// 	if err != nil {
	// 		return cRunSrv, err
	// 	}
	// 	fmt.Printf("\t%s\n", rev.Name)
	// }

	return cRunSrv, nil

}
