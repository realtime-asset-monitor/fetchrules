// Copyright 2024 Google LLC
//
// Licensed under the Apache License, Version 2.0 (the 'License');
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an 'AS IS' BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//.

package fetchrules

import (
	"bytes"
	"io"
	"log"
	"os"
	"strings"
	"testing"

	"cloud.google.com/go/storage"
	jsoniter "github.com/json-iterator/go"
	"google.golang.org/api/iterator"
)

func TestIntegUpdateRules2gcs(t *testing.T) {
	var testCases = []struct {
		name            string
		path            string
		onlyType        string
		wantMsgContains string
	}{
		{
			name: "rules03",
			path: "testdata/rules03",
		},
		{
			name:     "only_IAM_POLICY",
			path:     "testdata/rules01",
			onlyType: "IAM_POLICY",
		},
		{
			name:     "only_bigquery.googleapis.com/Dataset",
			path:     "testdata/rules01",
			onlyType: "bigquery.googleapis.com/Dataset",
		},
		{
			name: "rules01",
			path: "testdata/rules01",
		},
		{
			name:            "rules03-with-delete",
			path:            "testdata/rules03",
			wantMsgContains: "content or asset type deleted from the rules repo",
		},
	}

	projectID := os.Getenv("FETCHRULES_PROJECT_ID")
	if projectID == "" {
		t.Skip("Missing env var FETCHRULES_PROJECT_ID")
	}
	storageClient, err := storage.NewClient(ctx)
	if err != nil {
		log.Fatalf("storage.NewClient %v", err)
	}
	rulesRepoBucketName := projectID + "-rules2gcs"
	rulesRepoBucket := storageClient.Bucket(rulesRepoBucketName)
	query := &storage.Query{Prefix: ""}
	it := rulesRepoBucket.Objects(ctx, query)
	for {
		attrs, err := it.Next()
		if err == iterator.Done {
			break
		}
		if err != nil {
			log.Fatalf("it.Next() %s", err)
		}
		err = rulesRepoBucket.Object(attrs.Name).Delete(ctx)
		if err != nil {
			log.Fatalf("rulesRepoBucket.Object(attrs.Name).Delete(ctx) %s", err)
		}
		t.Logf("deleted test data %s %s", rulesRepoBucketName, attrs.Name)
	}

	for _, tc := range testCases {
		tc := tc // https://github.com/golang/go/wiki/CommonMistakes#using-goroutines-on-loop-iterator-variables
		t.Run(tc.name, func(t *testing.T) {
			constraints, rules, readmes, err := parseRuleFiles(tc.path)
			if err != nil {
				t.Fatalf("parseRuleFiles(tc.path) %v", err)
			}
			rr := makeRulesRepo(tc.onlyType, constraints, rules, readmes)
			var buffer bytes.Buffer
			log.SetOutput(&buffer)
			defer func() {
				log.SetOutput(os.Stderr)
			}()
			err = updateRules2gcs(ctx, tc.onlyType, rr, rulesRepoBucket, "INFO WARNING NOTICE CRITICAL")
			msgString := buffer.String()
			if err != nil {
				t.Errorf("want no errors and got %v", err)
			}
			if !strings.Contains(msgString, tc.wantMsgContains) {
				t.Errorf("want msg to contains '%s' and got \n'%s'", tc.wantMsgContains, msgString)
			}
			for contentOrAssetType, ruleConfigs := range rr.ContentOrAssetTypeConfigs {
				wantStr := "\"severity\":\"INFO\",\"message\":\"rules repo updated " + contentOrAssetType
				if !strings.Contains(msgString, wantStr) {
					t.Errorf("want msg to contains '%s' and got \n'%s'", wantStr, msgString)
				}

				reader, err := rulesRepoBucket.Object(contentOrAssetType).NewReader(ctx)
				if err != nil {
					t.Fatal(err)
				}
				b, err := io.ReadAll(reader)
				if err != nil {
					t.Fatal(err)
				}
				var ruleConfigsRetrieved RuleConfigs
				jsoniter.Unmarshal(b, &ruleConfigsRetrieved)

				for ruleName, ruleConfig := range ruleConfigs {
					if ruleConfigsRetrieved[ruleName].RegoCode != ruleConfig.RegoCode {
						t.Errorf("%s want regoCode '%s' and got \n'%s'", ruleName, ruleConfig.RegoCode, ruleConfigsRetrieved[ruleName].RegoCode)
					}
					if ruleConfigsRetrieved[ruleName].Version.UTC() != ruleConfig.Version.UTC() {
						t.Errorf("%s want version '%v' and got '%v'", ruleName, ruleConfig.Version.UTC(), ruleConfigsRetrieved[ruleName].Version.UTC())
					}
					if len(ruleConfigsRetrieved[ruleName].ConstraintConfigs) != len(ruleConfig.ConstraintConfigs) {
						t.Errorf("%s want '%d' constraints and got '%d'", ruleName, len(ruleConfig.ConstraintConfigs), len(ruleConfigsRetrieved[ruleName].ConstraintConfigs))

					}
				}
			}
			if tc.onlyType == "" {
				it := rulesRepoBucket.Objects(ctx, query)
				var countObj int
				for {
					_, err := it.Next()
					if err == iterator.Done {
						break
					}
					if err != nil {
						log.Fatalf("it.Next() %s", err)
					}
					countObj++
				}
				countTypes := len(rr.ContentOrAssetTypeConfigs)
				// t.Logf("countTypes %d countObj %d", countTypes, countObj)
				if countObj != countTypes {
					t.Errorf("want '%d' object in gcs got '%d'", countTypes, countObj)
				}
			}
		})
	}

}
