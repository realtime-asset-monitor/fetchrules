# Copyright 2024 Google LLC
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#      http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
package templates.gcp.GCPSQLLocationConstraintV1

import data.validator.gcp.lib as lib
import future.keywords.in

############################################
# Find Cloud SQL Location Violations
############################################
deny[{
	"msg": message,
	"details": metadata,
}] {
	constraint := input.constraint
	lib.get_constraint_params(constraint, params)

	# Verify that resource is Cloud SQL instance
	asset := input.asset
	asset.asset_type == "sqladmin.googleapis.com/Instance"

	# Check if resource is in exempt list
	exempt_list := lib.get_default(params, "exemptions", []) #  params.exemptions
	not asset.name in exempt_list

	# Check that location is in allowlist/denylist
	mode := lib.get_default(params, "mode", "allowlist")
	target_locations := lib.get_default(params, "locations", ["EU"])
	asset_location := asset.resource.data.region

	check_location(mode, asset_location, target_locations)

	message := sprintf("%v is in a disallowed location (%v).", [asset.name, asset_location])
	metadata := {"location": asset_location, "resource": asset.name}
}

#################
# Rule Utilities
#################

check_location(mode, asset_location, target_locations) {
	mode == "allowlist"
	not asset_location in target_locations
}

check_location(mode, asset_location, target_locations) {
	mode == "denylist"
	asset_location in target_locations
}
