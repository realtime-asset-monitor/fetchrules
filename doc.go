// Copyright 2024 Google LLC
//
// Licensed under the Apache License, Version 2.0 (the 'License');
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an 'AS IS' BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//

/*
Package fetchrules in the fetchrules go module is a function based on the function framework to fetch compliance rules related to the asset type

# Triggered by

A cloud event containing one asset config (the feed message)

# Output

- Cloud Events on PubSub assetRule topic.

# Cardinality

- n Cloud Events on PubSub assetRule topic. n = the number of matching rules

# Automatic retrying

Yes.
*/
package fetchrules
