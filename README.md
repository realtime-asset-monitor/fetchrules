# fetchrules

![pipeline status](https://gitlab.com/realtime-asset-monitor/fetchrules/badges/main/pipeline.svg) ![coverage](https://gitlab.com/realtime-asset-monitor/fetchrules/badges/main/coverage.svg?min_good=80)  
Fetch the compliance rules related to the asset type

## Prerequsites

Manual task:

- From Firestore console, select `native mode` and the multi-region location of your choice (e.g. eur3)

For running integration test in the DEV environment:

- `roles/datastore.user` instead of `roles/datastore.viewer`
