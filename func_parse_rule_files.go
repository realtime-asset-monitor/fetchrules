// Copyright 2024 Google LLC
//
// Licensed under the Apache License, Version 2.0 (the 'License');
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an 'AS IS' BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//.

package fetchrules

import (
	"os"
	"path/filepath"

	"gitlab.com/realtime-asset-monitor/utilities/str"
)

func parseRuleFiles(rulesFolderPath string) (constraints []constraint, rules regoRules, readmes readmeSet, err error) {
	rules = make(regoRules)
	readmes = make(readmeSet)
	err = filepath.Walk(rulesFolderPath, func(path string, info os.FileInfo, err error) error {
		if err != nil {
			return err
		}
		p := filepath.Clean(path) // prevent G304
		if !info.IsDir() && str.Find([]string{".rego", ".yml", ".yaml", ".md"}, filepath.Ext(path)) {
			// log.Printf("p %s ext %s", p, filepath.Ext(path))
			b, err := os.ReadFile(p)
			if err != nil {
				return err
			}
			switch filepath.Ext(path) {
			case ".yaml":
				constraints = getConstraint(p, b, constraints)
			case ".yml":
				constraints = getConstraint(p, b, constraints)
			case ".rego":
				rules = getRule(p, b, rules)
			case ".md":
				readmes = getReadme(p, b, readmes)
			}
		}
		return nil
	})
	return constraints, rules, readmes, err
}
