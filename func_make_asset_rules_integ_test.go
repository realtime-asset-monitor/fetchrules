// Copyright 2024 Google LLC
//
// Licensed under the Apache License, Version 2.0 (the 'License');
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an 'AS IS' BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//.

package fetchrules

import (
	"context"
	"encoding/json"
	"fmt"
	"log"
	"os"
	"path/filepath"
	"reflect"
	"strings"
	"testing"

	"cloud.google.com/go/storage"
	"gitlab.com/realtime-asset-monitor/utilities/cai"
	"google.golang.org/api/iterator"
)

func TestIntegMakeAssetRules(t *testing.T) {
	var testCases = []struct {
		name                     string
		feedPath                 string
		rulesFolderPath          string
		onlyType                 string
		callTwiceToPopulateCache bool
		wantRuleCount            int
		wantContentOrAssetTypes  []string
		wantRuleNames            []string
		wantConstraintCount      int
	}{
		{
			name:                    "feed_iam_policy",
			feedPath:                "testdata/asset_feed_02.json",
			rulesFolderPath:         "testdata/rules99",
			wantRuleCount:           2,
			wantContentOrAssetTypes: []string{"IAM_POLICY"},
			wantRuleNames:           []string{"GCPIAMMembersConstraintV1", "GCPIAMBindingsrolesConstraintV1"},
			wantConstraintCount:     7,
		},
		{
			name:                     "feed_resource",
			feedPath:                 "testdata/asset_feed_01.json",
			rulesFolderPath:          "testdata/rules99",
			callTwiceToPopulateCache: true,
			wantRuleCount:            2,
			wantContentOrAssetTypes:  []string{"bigquery.googleapis.com/Dataset", "bigquery.googleapis.com/DatasetRESOURCE"},
			wantRuleNames:            []string{"GCPBigQueryDatasetLocationConstraintV1"},
			wantConstraintCount:      2,
		},
		{
			name:                    "feed_unknown",
			feedPath:                "testdata/asset_feed_03.json",
			rulesFolderPath:         "testdata/rules99",
			wantRuleCount:           0,
			wantContentOrAssetTypes: []string{},
		},
		{
			name:                    "case_content_type_and_asset_type",
			feedPath:                "testdata/asset_feed_04.json",
			rulesFolderPath:         "testdata/rules99",
			wantRuleCount:           3,
			wantContentOrAssetTypes: []string{"IAM_POLICY", "run.googleapis.com/ServiceIAM_POLICY"},
			wantRuleNames:           []string{"GCPIAMMembersConstraintV1", "GCPIAMBindingsrolesConstraintV1", "CloudRunIngressAndIAMRoleV1"},
			wantConstraintCount:     8,
		},
	}
	projectID := os.Getenv("FETCHRULES_PROJECT_ID")
	if projectID == "" {
		t.Skip("Missing env var FETCHRULES_PROJECT_ID")
	}
	ctx := context.Background()
	storageClient, err := storage.NewClient(ctx)
	if err != nil {
		log.Fatalf("storage.NewClient %v", err)
	}
	rulesRepoBucketName := projectID + "-makeassetrules"
	rulesRepoBucket := storageClient.Bucket(rulesRepoBucketName)

	for _, tc := range testCases {
		tc := tc // prevent G601
		t.Run(tc.name, func(t *testing.T) {
			if tc.rulesFolderPath != "" {
				query := &storage.Query{Prefix: ""}
				it := rulesRepoBucket.Objects(ctx, query)
				for {
					attrs, err := it.Next()
					if err == iterator.Done {
						break
					}
					if err != nil {
						log.Fatalf("it.Next() %s", err)
					}
					err = rulesRepoBucket.Object(attrs.Name).Delete(ctx)
					if err != nil {
						log.Fatalf("rulesRepoBucket.Object(attrs.Name).Delete(ctx) %s", err)
					}
					t.Logf("deleted test data %s %s", rulesRepoBucketName, attrs.Name)
				}
				constraints, rules, readmes, err := parseRuleFiles(tc.rulesFolderPath)
				if err != nil {
					t.Fatalf("parseRuleFiles(tc.path) %v", err)
				}
				rr := makeRulesRepo("", constraints, rules, readmes)
				err = updateRules2gcs(ctx, tc.onlyType, rr, rulesRepoBucket, "INFO WARNING NOTICE CRITICAL")
				if err != nil {
					t.Fatalf("updateRules2gcs %v", err)
				}

				p := filepath.Clean(tc.feedPath)
				if !strings.HasPrefix(p, "testdata/") {
					panic(fmt.Errorf("Unsafe path %s", p)) // prevent G304
				}
				b, err := os.ReadFile(p)
				if err != nil {
					log.Fatalln(err)
				}
				var caiFeedMsg cai.FeedMessage
				json.Unmarshal(b, &caiFeedMsg)
				if err != nil {
					log.Fatalln(err)
				}
				contentOrAssetTypes, assetRules, err := makeAssetRules(ctx, &caiFeedMsg, rulesRepoBucket)
				_ = err
				if tc.callTwiceToPopulateCache {
					contentOrAssetTypes, assetRules, err = makeAssetRules(ctx, &caiFeedMsg, rulesRepoBucket)
					_ = err
				}

				if len(assetRules) != tc.wantRuleCount {
					t.Errorf("want %d rules and got %d", tc.wantRuleCount, len(assetRules))
				}
				if !reflect.DeepEqual(contentOrAssetTypes, tc.wantContentOrAssetTypes) {
					t.Errorf("want contentOrAssetTypes %v and got %v", tc.wantContentOrAssetTypes, contentOrAssetTypes)
				}

				var ruleNames string
				var countConstraints int
				for _, assetRule := range assetRules {
					ruleNames = ruleNames + " " + assetRule.Rule.Name
					if len(assetRule.Rule.RegoModules) != 4 {
						t.Errorf("want 4 rego module per rule and got %d", len(assetRule.Rule.RegoModules))
					}
					countConstraints = countConstraints + len(assetRule.Rule.Constraints)
					for regoModuleName := range assetRule.Rule.RegoModules {
						if !strings.HasSuffix(regoModuleName, ".rego") {
							t.Errorf("want regoModuleName with suffix .rego and got %s", regoModuleName)
						}
					}
					_, ok := assetRule.Rule.RegoModules["audit.rego"]
					if !ok {
						t.Errorf("want audit.rego, got %v", assetRule.Rule.RegoModules)
					}
					_, ok = assetRule.Rule.RegoModules["constraints.rego"]
					if !ok {
						t.Errorf("want constraints.rego, got %v", assetRule.Rule.RegoModules)
					}
					_, ok = assetRule.Rule.RegoModules["util.rego"]
					if !ok {
						t.Errorf("want util.rego, got %v", assetRule.Rule.RegoModules)
					}
				}
				for _, rName := range tc.wantRuleNames {
					if !strings.Contains(ruleNames, rName) {
						t.Errorf("want ruleName %s and did not get it", rName)
					}
				}
				if countConstraints != tc.wantConstraintCount {
					t.Errorf("want %d constraints and got %d", tc.wantConstraintCount, countConstraints)
				}
			}
		})
	}
}
