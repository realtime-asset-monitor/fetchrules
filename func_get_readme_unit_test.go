// Copyright 2024 Google LLC
//
// Licensed under the Apache License, Version 2.0 (the 'License');
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an 'AS IS' BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//.

package fetchrules

import (
	"bytes"
	"fmt"
	"log"
	"os"
	"path/filepath"
	"strings"
	"testing"
)

func TestUnitGetReadme(t *testing.T) {
	var testCases = []struct {
		name            string
		path            string
		want            string
		wantMsgContains string
		countChanges    int
	}{
		{
			name:         "gcf_location_europe",
			path:         "testdata/rules01/gcf/gcf_location_europe/readme.md",
			want:         "gcf_location_europe",
			countChanges: 1,
		},
		{
			name:            "any_readme",
			path:            "testdata/any.md",
			wantMsgContains: "ignored not readme.md",
			countChanges:    0,
		},
	}
	for _, tc := range testCases {
		tc := tc // https://github.com/golang/go/wiki/CommonMistakes#using-goroutines-on-loop-iterator-variables
		t.Run(tc.name, func(t *testing.T) {
			// t.Parallel()
			p := filepath.Clean(tc.path)
			if !strings.HasPrefix(p, "testdata/") {
				panic(fmt.Errorf("Unsafe path %s", p)) // prevent G304
			}
			b, err := os.ReadFile(p)
			if err != nil {
				log.Fatalln(err)
			}
			readmes := make(readmeSet)
			var buffer bytes.Buffer
			log.SetOutput(&buffer)
			defer func() {
				log.SetOutput(os.Stderr)
			}()
			readmes = getReadme(p, b, readmes)
			msgString := buffer.String()

			if tc.want != "" {
				if _, ok := readmes[tc.want]; !ok {
					t.Errorf("Want %s in readmes map and did not get it", tc.want)
				}
			}
			if !strings.Contains(msgString, tc.wantMsgContains) {
				t.Errorf("want msg to contains '%s' and got \n'%s'", tc.wantMsgContains, msgString)
			}
			if len(readmes) != tc.countChanges {
				t.Errorf("want %d changes in readmes map and got %d", tc.countChanges, len(readmes))
			}
		})
	}
}
