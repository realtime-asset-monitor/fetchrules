// Copyright 2024 Google LLC
//
// Licensed under the Apache License, Version 2.0 (the 'License');
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an 'AS IS' BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//.

package fetchrules

import (
	"context"
	"errors"
	"fmt"
	"io"
	"time"

	"cloud.google.com/go/storage"
	jsoniter "github.com/json-iterator/go"
	"gitlab.com/realtime-asset-monitor/utilities/cai"
	"google.golang.org/api/googleapi"
)

func getAssetRules(ctx context.Context,
	contentOrAssetType string,
	caiFeedMsg *cai.FeedMessage,
	rulesRepoBucket *storage.BucketHandle) (assetRules []cai.AssetRule, err error) {
	assetRules = make([]cai.AssetRule, 0)
	var foundInCache, formatOk, isTooOld bool
	var typeConfig contentOrAssetTypeConfig
	var contentOrAssetTypeConfigInterface interface{}
	var ruleConfigs RuleConfigs
	contentOrAssetTypeConfigInterface, foundInCache = global.cacheContentOrAssetTypeConfigs.Get(contentOrAssetType)
	if foundInCache {
		if typeConfig, formatOk = contentOrAssetTypeConfigInterface.(contentOrAssetTypeConfig); formatOk {
			now := time.Now()
			cachedItemAge := now.Sub(typeConfig.version)
			if global.serviceEnv.CacheMaxAgeMinutes == 0 || cachedItemAge.Minutes() > global.serviceEnv.CacheMaxAgeMinutes {
				isTooOld = true
			} else {
				ruleConfigs = typeConfig.ruleConfigs
			}
		} else {
			return assetRules, fmt.Errorf("unexpected structure in cache contentorassettypeconfiginterface.(contentorassettypeconfig)")
		}
	}

	if isTooOld || !foundInCache {
		var errCode int
		reader, err := rulesRepoBucket.Object(contentOrAssetType).NewReader(ctx)
		if err != nil {
			var e *googleapi.Error
			if ok := errors.As(err, &e); ok {
				if e.Code == 404 {
					errCode = e.Code
				}
			}
			if errCode != 404 {
				return assetRules, fmt.Errorf("rulesRepoBucket.Object(contentOrAssetType).NewReader(ctx) %v", err)
			}
		}
		typeConfig.version = time.Now()
		if errCode == 404 {
			typeConfig.foundInGCS = false
		} else {
			typeConfig.foundInGCS = true
			defer reader.Close()
			b, err := io.ReadAll(reader)
			if err != nil {
				return assetRules, fmt.Errorf("io.ReadAll(reader) %v", err)
			}
			err = jsoniter.Unmarshal(b, &ruleConfigs)
			if err != nil {
				return assetRules, fmt.Errorf("jsoniter.Unmarshal %v", err)
			}
			typeConfig.ruleConfigs = ruleConfigs
		}
		global.cacheContentOrAssetTypeConfigs.Set(contentOrAssetType, typeConfig)
	}

	for ruleName, ruleConfig := range ruleConfigs {
		var assetRule cai.AssetRule
		assetRule.FeedMessage = *caiFeedMsg
		assetRule.Rule.Name = ruleName
		assetRule.Rule.DeploymentTime = ruleConfig.Version
		assetRule.Rule.RegoModules = make(map[string]string)
		assetRule.Rule.RegoModules[ruleName+".rego"] = ruleConfig.RegoCode
		for f, c := range global.commonRego {
			assetRule.Rule.RegoModules[f] = c
		}
		// assetRule.Rule.RegoModules["audit.rego"] = auditRego
		// assetRule.Rule.RegoModules["constraints.rego"] = constraintsRego
		// assetRule.Rule.RegoModules["util.rego"] = utilRego
		for _, constraintConfig := range ruleConfig.ConstraintConfigs {
			constraintConfig := constraintConfig
			assetRule.Rule.Constraints = append(assetRule.Rule.Constraints, constraintConfig)
		}
		assetRules = append(assetRules, assetRule)
	}
	return assetRules, err

}
