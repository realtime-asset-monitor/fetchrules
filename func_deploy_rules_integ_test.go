// Copyright 2024 Google LLC
//
// Licensed under the Apache License, Version 2.0 (the 'License');
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an 'AS IS' BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//.

package fetchrules

import (
	"errors"
	"fmt"
	"log"
	"os"
	"strings"
	"testing"

	"cloud.google.com/go/storage"
	"google.golang.org/api/iterator"
)

func TestIntegDeployRules(t *testing.T) {
	var testCases = []struct {
		name                  string
		rulesFolderPath       string
		onlyType              string
		logOnlySeverityLevels string
		wantErrorContains     string
	}{
		{
			name:              "not_folder",
			wantErrorContains: "no such file or directory",
		},
		{
			name:            "rules01",
			rulesFolderPath: "testdata/rules01",
		},
	}
	projectID := os.Getenv("FETCHRULES_PROJECT_ID")
	if projectID == "" {
		t.Skip("Missing env var FETCHRULES_PROJECT_ID")
	}
	storageClient, err := storage.NewClient(ctx)
	if err != nil {
		log.Fatalf("storage.NewClient %v", err)
	}
	rulesRepoBucketName := projectID + "-makeassetrules"
	rulesRepoBucket := storageClient.Bucket(rulesRepoBucketName)

	for _, tc := range testCases {
		tc := tc // https://github.com/golang/go/wiki/CommonMistakes#using-goroutines-on-loop-iterator-variables
		t.Run(tc.name, func(t *testing.T) {
			query := &storage.Query{Prefix: ""}
			it := rulesRepoBucket.Objects(ctx, query)
			for {
				attrs, err := it.Next()
				if err == iterator.Done {
					break
				}
				if err != nil {
					log.Fatalf("it.Next() %s", err)
				}
				err = rulesRepoBucket.Object(attrs.Name).Delete(ctx)
				if err != nil {
					log.Fatalf("rulesRepoBucket.Object(attrs.Name).Delete(ctx) %s", err)
				}
				t.Logf("deleted test data %s %s", rulesRepoBucketName, attrs.Name)
			}

			err := DeployRules(ctx,
				tc.rulesFolderPath,
				tc.onlyType,
				rulesRepoBucketName,
				projectID,
				tc.logOnlySeverityLevels,
				false,
				"")

			if err != nil {
				if tc.wantErrorContains != "" {
					if !strings.Contains(err.Error(), tc.wantErrorContains) {
						t.Errorf("want msg to contains '%s' and got \n'%s'", tc.wantErrorContains, err.Error())
					}
				} else {
					t.Errorf("want no errors and got %v", err)
				}
			} else {
				if tc.wantErrorContains != "" {
					t.Errorf("want error %s and got none", tc.wantErrorContains)
				}
				fi, err := os.Stat(fmt.Sprintf("%s/rulerepo.json", tc.rulesFolderPath))
				if err != nil {
					if errors.Is(err, os.ErrNotExist) {
						t.Errorf("want exist file ./rulerepo.json and it does not")
					} else {
						t.Errorf("%v", err)
					}
				} else {
					if fi.Size() == 0 {
						t.Error("want file ./rulerepo.json with content and was 0 size")
					}
				}
				if errors.Is(err, os.ErrNotExist) {
					t.Errorf("want exist file ./rulerepo.json and it does not")
				}
			}

		})
	}
}
